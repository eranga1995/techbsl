package Module.Student;

import Core.Core;
import com.github.javafaker.Faker;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import java.util.Random;

public class Student {
    private String user = "amila@tmail.com";
    private String password = "123456";

    @Test
    public void addStudent() {
        Core obj = new Core();
        try {
            System.setProperty(obj.getDriverPath(), obj.getLocalPath());
            WebDriver driver = new ChromeDriver();
            driver.manage().window().maximize();
            driver.get(obj.getBaseUrl());
            Thread.sleep(1000);

            WebElement schoolType = driver.findElement(By.id("empDiv"));
            schoolType.click();
            WebElement username = driver.findElement(By.name("username"));
            username.sendKeys(user);
            WebElement pwd = driver.findElement(By.name("password"));
            pwd.sendKeys(password);
            WebElement login = driver.findElement(By.id("submit"));
            login.click();
            Thread.sleep(4000);

            WebElement selectModule = driver.findElement(By.id("Student"));
            selectModule.click();
            WebElement clickStudentDetails = driver.findElement(By.id("2"));
            clickStudentDetails.click();
            Thread.sleep(2000);
            WebElement addBtn = driver.findElement(By.id("stuAddBtn"));
            addBtn.click();
            Thread.sleep(2000);
            // insert form
            WebElement stuID = driver.findElement(By.id("serialno"));
            String ID = String.valueOf(getStudentID());
            stuID.sendKeys(ID);
            Thread.sleep(2000);
            // use faker
            Faker faker = new Faker();
            WebElement firstName = driver.findElement(By.id("fname"));
            firstName.sendKeys(getFirstName());
            WebElement middleName = driver.findElement(By.id("mName"));
            middleName.sendKeys(getMiddleName());
            WebElement lastName = driver.findElement(By.id("surname"));
            lastName.sendKeys(getLastName());
            WebElement commonName = driver.findElement(By.id("othername"));
            commonName.sendKeys(firstName.getAttribute("value"));
            // gender dropdown selection
            WebElement dropDown = driver.findElement(By.id("gender"));
            Select droped = new Select(dropDown);
            droped.selectByIndex(getIndexForGenderDropDown());
            // select DOB from date time picker
            WebElement dob = driver.findElement(By.id("dob"));
            dob.sendKeys("09222022");
            WebElement stuCategory = driver.findElement(By.id("StuCategory"));
            Select sCategory = new Select(stuCategory);
            sCategory.selectByIndex(1);
            Thread.sleep(4000);

            driver.quit();
        } catch(Exception e) {
            System.out.println("Logical Error = " + e);
        }
    }

    public int getStudentID() {
        Random random = new Random();
        int number = random.nextInt();
        return number;
    }

    public String getFirstName() {
        Faker faker = new Faker();
        return faker.name().firstName();
    }

    public String getMiddleName() {
        Faker faker = new Faker();
        return faker.name().nameWithMiddle();
    }

    public String getLastName() {
        Faker faker = new Faker();
        return faker.name().lastName();
    }

    public int getIndexForGenderDropDown() {
        Random ran = new Random();
        Core obj = new Core();
        int[] arr = obj.getStudentIDGenerateMinMax();
        int number = ran.nextInt(arr[0], arr[1]);
        return number;
    }

    public int getIndexForSelectStudentCategory() {
        Random ran = new Random();
        int index = ran.nextInt(1, 2);
        return index;
    }
}