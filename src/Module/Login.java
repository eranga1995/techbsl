package Module;

import Core.Core;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Login {
    //Login Credentials
    @DataProvider(name = "singleUser")
    public Object[][] getData() {
        return new Object[][] {
                {"bothCorrect", "amila@tmail.com", "123456"}
        };
    }
    @DataProvider(name = "multipleUser")
    public  Object[][] getMultipleUser() {
        return new Object[][] {
                {"usernameCorrect", "amila@tmail.com", "1234567"},
                {"bothCorrect", "amila@tmail.com", "123456"},
                {"passwordCorrect", "amila@gmail.com", "123456"},
                {"bothWrong", "amila@gmail.com", "1234567"}
        };
    }

    @Test(dataProvider = "multipleUser")
    public void verifyLoginWithValidation(String scenario, String username, String password) {
        Core obj = new Core();
        try {
            System.setProperty(obj.getDriverPath(), obj.getLocalPath());
            WebDriver driver = new ChromeDriver();
            driver.manage().window().maximize();
            driver.get(obj.getBaseUrl());
            Thread.sleep(2000);

            WebElement schoolType = driver.findElement(By.id("empDiv"));
            schoolType.click();
            WebElement userName = driver.findElement(By.name("username"));
            userName.sendKeys(username);
            WebElement pwd = driver.findElement(By.name("password"));
            pwd.sendKeys(password);
            WebElement login = driver.findElement(By.id("submit"));
            login.click();
            Thread.sleep(3000);

            if (scenario.equals("bothCorrect")) {
                String expectedUrl = driver.getCurrentUrl();
                Assert.assertEquals(expectedUrl, "https://exqa.techbsl.com/index.php/Srp_ha_empDashboardController");
            } else if (scenario.equals("usernameCorrect")) {
                String expectedUrl = driver.getCurrentUrl();
                Assert.assertEquals(expectedUrl, obj.getBaseUrl());
            } else if (scenario.equals("passwordCorrect")) {
                String expectedUrl = driver.getCurrentUrl();
                Assert.assertEquals(expectedUrl, obj.getBaseUrl());
            } else {
                String expectedUrl = driver.getCurrentUrl();
                Assert.assertEquals(expectedUrl, obj.getBaseUrl());
            }

            driver.quit();
        } catch(Exception e) {
            System.out.println("Logical Error = " + e);
        }
    }
}
